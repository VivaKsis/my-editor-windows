﻿using UnityEngine;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using Sirenix.OdinInspector.Editor;
using UnityEditor;

public class TimeContainerDrawer : OdinValueDrawer<TimeContainer> {

    private InspectorProperty min;
    private InspectorProperty sec;

    protected override void Initialize() {
        min = this.Property.Children["min"];
        sec = this.Property.Children["sec"];
    }

    protected override void DrawPropertyLayout(GUIContent label) {

        Rect rect = EditorGUILayout.GetControlRect();

        GUIContent labelMin = new GUIContent("min");
        GUIContent labelSec = new GUIContent("sec");

        float rectWidth = 80f;
        float suffixLabelWidth = 30f;

        if (label != null)
            rect = EditorGUI.PrefixLabel(rect, label);

        rect = RectExtensions.MaxWidth(rect, rectWidth);

        min.ValueEntry.WeakSmartValue = SirenixEditorFields.IntField(rect,
            "", (int)min.ValueEntry.WeakSmartValue);

        rect.x += rectWidth;

        GUI.Label(rect, labelMin);

        rect.x += suffixLabelWidth;

        sec.ValueEntry.WeakSmartValue = SirenixEditorFields.IntField(rect,
            "", (int)sec.ValueEntry.WeakSmartValue);

        rect.x += rectWidth;

        GUI.Label(rect, labelSec);
    }
}
