﻿using Sirenix.OdinInspector;
using System;
using UnityEngine;

[CreateAssetMenu(fileName = "EventData", menuName = "EventCreator/EventData")]
public class EventData : SOData {

    #region EventGeneral

    [BoxGroup("Event")]
    [ShowIf("@eventType == EventType.spawnBoss || eventType == EventType.spawnSquad"), HorizontalGroup("Event/fname", 0.35f), ReadOnly, LabelText("Name"), LabelWidth(65), GUIColor(0.8f, 0.8f, 0.8f)]
    public string factionName = "Treks";
    [ShowIf("@eventType == EventType.spawnSquad"), HorizontalGroup("Event/fname", 0.05f), ReadOnly, HideLabel, GUIColor(0.8f, 0.8f, 0.8f)]
    public string difficultyName = "e";
    [ShowIf("@eventType == EventType.spawnBoss || eventType == EventType.spawnSquad"), HorizontalGroup("Event/fname", 0.65f), HideLabel, GUIColor(0.8f, 0.8f, 0.8f)]
    public string enemyName;
    [HideIf("@eventType == EventType.spawnBoss || eventType == EventType.spawnSquad"), BoxGroup("Event"), LabelText("Name"), GUIColor("GetColorEventName")]
    public string eventName;
    [BoxGroup("Event"), OnValueChanged("EventTypeManagement"), LabelText("Type"), GUIColor(0.8f, 0.8f, 0.8f)]
    public EventType eventType;

    #endregion

    #region SpawnItem

    [HorizontalGroup("$GetParametersTitle/Left", 75), ShowIf("@eventType == EventType.spawnItem"), PreviewField(75), HideLabel, GUIColor("GetColorItemPrefab")]
    public UnityEngine.Object itemPrefab;

    #endregion

    #region SpawnSquad

    [BoxGroup("$GetParametersTitle"), OnValueChanged("SetFactionName"), LabelText("Faction"), ShowIf("@eventType == EventType.spawnSquad"), GUIColor(0.8f, 0.8f, 0.8f)]
    public Faction squadFaction;

    [BoxGroup("$GetParametersTitle"), LabelText("Power Level"), ShowIf("@eventType == EventType.spawnSquad"), GUIColor("GetColorSquadPowerLevel")]
    public int squadPowerLevel;

    [BoxGroup("$GetParametersTitle"), OnValueChanged("SetDifficultyname"), ShowIf("@eventType == EventType.spawnSquad"), GUIColor(0.8f, 0.8f, 0.8f)]
    public Difficulty patternDifficulty;

    [BoxGroup("$GetParametersTitle"), ShowIf("@eventType == EventType.spawnSquad"), GUIColor("GetColorSquadUnitCount")]
    public int unitCount;

    [BoxGroup("$GetParametersTitle"), ShowIf("@eventType == EventType.spawnSquad"), GUIColor("GetColorSquadEntryDuration")]
    public float spawnEntryDuration;

    [BoxGroup("$GetParametersTitle"), ShowIf("@eventType == EventType.spawnSquad"), GUIColor(0.8f, 0.8f, 0.8f)]
    public bool stopSpawnerUntilSquadDestroyed;

    [BoxGroup("$GetParametersTitle"), LabelText("Approximate Time"), ShowIf("@eventType == EventType.spawnSquad"), GUIColor("GetColorSquadTime")]
    public TimeContainer approxSpawnSquadTime = new TimeContainer();

    [BoxGroup("$GetParametersTitle"), LabelText("Timeline Prefab"), ShowIf("@eventType == EventType.spawnSquad"), GUIColor("GetColorSquadPrefab")]
    public UnityEngine.Object squadTimelinePrefab;

    [BoxGroup("$GetParametersTitle"), ShowIf("@eventType == EventType.spawnSquad"), PropertySpace(SpaceBefore = 20), InlineProperty]
    public LanesNumbers squadLanesNumbers = new LanesNumbers(216f);
    [BoxGroup("$GetParametersTitle"), LabelText("Default Spawners"), LabelWidth(220), ShowIf("@eventType == EventType.spawnSquad"), InlineProperty, GUIColor(0.8f, 0.8f, 0.8f)]
    public Lanes squadLanes = new Lanes();

    #endregion

    #region SpawnBoss

    [BoxGroup("$GetParametersTitle"), OnValueChanged("SetFactionName"), LabelText("Faction"), ShowIf("@eventType == EventType.spawnBoss"), GUIColor(0.8f, 0.8f, 0.8f)]
    public Faction bossFaction;

    [BoxGroup("$GetParametersTitle"), LabelText("Power Level"), ShowIf("@eventType == EventType.spawnBoss"), GUIColor("GetColorBossPowerLevel")]
    public int bossPowerLevel;

    [BoxGroup("$GetParametersTitle"), LabelText("Approximate Time"), ShowIf("@eventType == EventType.spawnBoss"), GUIColor("GetColorBossTime")]
    public TimeContainer approxSpawnBossTime = new TimeContainer();

    [BoxGroup("$GetParametersTitle"), LabelText("Timeline Prefab"), ShowIf("@eventType == EventType.spawnBoss"), GUIColor("GetColorBossPrefab")]
    public UnityEngine.Object bossTimelinePrefab;

    [BoxGroup("$GetParametersTitle"), ShowIf("@eventType == EventType.spawnBoss"), PropertySpace(SpaceBefore = 20), InlineProperty]
    public LanesNumbers bossLanesNumbers = new LanesNumbers(216f);
    [BoxGroup("$GetParametersTitle"), LabelText("Default Spawners"), LabelWidth(220), ShowIf("@eventType == EventType.spawnBoss"), InlineProperty, GUIColor(0.8f, 0.8f, 0.8f)]
    public Lanes bossLanes = new Lanes();


    #endregion

    #region SpawnObstacle

    [BoxGroup("$GetParametersTitle"), ShowIf("@eventType == EventType.spawnObstacle"), GUIColor(0.8f, 0.8f, 0.8f)]
    public bool dropResource;

    [BoxGroup("$GetParametersTitle"), LabelText("Power Level"), ShowIf("@eventType == EventType.spawnObstacle"), GUIColor("GetColorObstaclePowerLevel")]
    public int obstaclePowerLevel;

    [BoxGroup("$GetParametersTitle"), LabelText("Approximate Time"), ShowIf("@eventType == EventType.spawnObstacle"), GUIColor("GetColorObstacleTime")]
    public TimeContainer approxObstacleTime = new TimeContainer();

    [BoxGroup("$GetParametersTitle"), LabelText("Timeline Prefab"), ShowIf("@eventType == EventType.spawnObstacle"), GUIColor("GetColorObstaclePrefab")]
    public UnityEngine.Object obstacleTimelinePrefab;

    #endregion

    #region ChangeSpeed

    [BoxGroup("$GetParametersTitle"), LabelText("Option"), ShowIf("@eventType == EventType.changeSpeed"), GUIColor(0.8f, 0.8f, 0.8f)]
    public ChangeSpeedOption changeSpeedOption;
    [BoxGroup("$GetParametersTitle"), LabelText("Speed Modifier"), ShowIf("@eventType == EventType.changeSpeed && (changeSpeedOption == ChangeSpeedOption.increaseTime || changeSpeedOption == ChangeSpeedOption.decreaseTime)"), GUIColor("GetColorChangeTimeModifier")]
    public float changeSpeedModifier;

    #endregion

    #region Multiplier

    [BoxGroup("$GetParametersTitle"), LabelText("Message"), ReadOnly, ShowIf("@eventType == EventType.changeMultiplier"), GUIColor(0.8f, 0.8f, 0.8f)]
    public string multiplierMessage = "Coming soon";

    #endregion

    #region Dialog

    [BoxGroup("$GetParametersTitle"), ReadOnly, ShowIf("@eventType == EventType.dialog"), GUIColor(0.8f, 0.8f, 0.8f)]
    public string character = "Coming Soon (Will be Enum)";

    [BoxGroup("$GetParametersTitle"), LabelText("Image"), ShowIf("@eventType == EventType.dialog"), GUIColor("GetColorDialogImage")]
    public Sprite dialogImage;

    [BoxGroup("$GetParametersTitle"), LabelText("Approximate Time"), ShowIf("@eventType == EventType.dialog"), GUIColor("GetColorDialogTime")]
    public TimeContainer approxDialogTime = new TimeContainer();

    [BoxGroup("$GetParametersTitle"), LabelText("Type"), ShowIf("@eventType == EventType.dialog"), GUIColor(0.8f, 0.8f, 0.8f)]
    public DialogType dialogType;

    [BoxGroup("$GetParametersTitle"), LabelText("Text"), ShowIf("@eventType == EventType.dialog"), TextArea, GUIColor("GetColorDialogText")]
    public string dialogText;

    #endregion

    #region UniqueEvent

    [BoxGroup("$GetParametersTitle"), LabelText("Message"), ReadOnly, ShowIf("@eventType == EventType.uniqueEvent"), GUIColor(0.8f, 0.8f, 0.8f)]
    public string uniqueEventMessage = "Coming soon";

    #endregion

    #region Paths
    private string changeMultiplierPath = "Assets/Resources/Events/Change Multiplier";
    private string changeSpeedPath = "Assets/Resources/Events/Change Speed";
    private string dialogPath = "Assets/Resources/Events/Dialog";
    private string spawnBossPath = "Assets/Resources/Events/Spawn Boss";
    private string spawnItemPath = "Assets/Resources/Events/Spawn Item";
    private string spawnObstaclePath = "Assets/Resources/Events/Spawn Obstacle";
    private string spawnSquadPath = "Assets/Resources/Events/Spawn Squad";
    private string uniqueEventPath = "Assets/Resources/Events/Unique Event";
    #endregion

    #region ChangeTypeManagement

    [HideInInspector]
    public ChangeTypeConfirmer changeTypeConfirmer;

    private EventType backUpType = EventType.spawnItem;
    private EventType chosenType;
    private bool isAnyDataOnPage;

    private bool isEventTypeManagementRunning;

    private void EventTypeManagement() {
        if (isEventTypeManagementRunning) {
            return;
        }
        isEventTypeManagementRunning = true;
        if (changeTypeConfirmer == null) {
            isAnyDataOnPage = CheckForDataInput();
            if (isAnyDataOnPage) {
                PopUpChangeTypeWarningWindow();
            }
            else {
                backUpType = eventType;
            }
        }
        isEventTypeManagementRunning = false;
    }

    private bool CheckForDataInput() {
        if (eventName != null) {
            return true;
        }
        switch (backUpType) {
            case EventType.spawnItem:
                if (itemPrefab != null) {
                    return true;
                }
                else {
                    return false;
                }
            case EventType.spawnSquad:
                if (squadPowerLevel != 0) {
                    return true;
                }
                else if (unitCount != 0) {
                    return true;
                }
                else if (spawnEntryDuration != 0) {
                    return true;
                }
                else if (stopSpawnerUntilSquadDestroyed) {
                    return true;
                }
                else if (approxSpawnSquadTime.min != 0 && approxSpawnSquadTime.sec != 0) {
                    return true;
                }
                else if (squadTimelinePrefab != null) {
                    return true;
                }
                else {
                    return false;
                }
            case EventType.spawnBoss:
                if (bossPowerLevel != 0) {
                    return true;
                }
                else if (approxSpawnBossTime.min != 0 && approxSpawnBossTime.sec != 0) {
                    return true;
                }
                else if (bossTimelinePrefab != null) {
                    return true;
                }
                else {
                    return false;
                }
            case EventType.spawnObstacle:
                if (dropResource) {
                    return true;
                }
                else if (obstaclePowerLevel != 0) {
                    return true;
                }
                else if (approxObstacleTime.min != 0 && approxObstacleTime.sec != 0) {
                    return true;
                }
                else if (obstacleTimelinePrefab != null) {
                    return true;
                }
                else {
                    return false;
                }
            case EventType.changeSpeed:
                if (changeSpeedOption == ChangeSpeedOption.decreaseTime || changeSpeedOption == ChangeSpeedOption.increaseTime) {
                    if (changeSpeedModifier != 0) {
                        return true;
                    }
                }
                return false;
            case EventType.changeMultiplier:
                return false;
            case EventType.dialog:
                if (dialogImage) {
                    return true;
                }
                else if (approxDialogTime.min != 0 && approxDialogTime.sec != 0) {
                    return true;
                }
                else if (dialogText != null) {
                    return true;
                }
                else {
                    return false;
                }
            case EventType.uniqueEvent:
                return false;
            default:
                return false;
        }
    }

    private void PopUpChangeTypeWarningWindow() {
        CreatePopUpWindow(ref changeTypeConfirmer);
        if (changeTypeConfirmer != null && changeTypeConfirmer.window != null) {
            changeTypeConfirmer.window.OnClose += CheckIfConfirmed;
        }
        chosenType = eventType;
        eventType = backUpType;
    }

    private void CheckIfConfirmed() {
        if (changeTypeConfirmer.isOk) {
            ChangeTypeToChosen();
        }
        changeTypeConfirmer = null;
    }

    private void ChangeTypeToChosen() {
        eventType = chosenType;
        ClearEachVariable();
    }

    private void ClearEachVariable() {

        switch (backUpType) {
            case EventType.spawnItem:
                if (itemPrefab != null) {
                    itemPrefab = null;
                }
                break;
            case EventType.spawnSquad:
                if (squadPowerLevel != 0) {
                    squadPowerLevel = 0;
                }
                if (unitCount != 0) {
                    unitCount = 0;
                }
                if (spawnEntryDuration != 0) {
                    spawnEntryDuration = 0;
                }
                if (stopSpawnerUntilSquadDestroyed) {
                    stopSpawnerUntilSquadDestroyed = false;
                }
                if (approxSpawnSquadTime != null) {
                    approxSpawnSquadTime = null;
                }
                if (squadTimelinePrefab != null) {
                    squadTimelinePrefab = null;
                }
                break;
            case EventType.spawnBoss:
                if (bossPowerLevel != 0) {
                    bossPowerLevel = 0;
                }
                if (approxSpawnBossTime != null) {
                    approxSpawnBossTime = null;
                }
                if (bossTimelinePrefab != null) {
                    bossTimelinePrefab = null;
                }
                break;
            case EventType.spawnObstacle:
                if (dropResource) {
                    dropResource = true;
                }
                if (obstaclePowerLevel != 0) {
                    obstaclePowerLevel = 0;
                }
                if (approxObstacleTime != null) {
                    approxObstacleTime = null;
                }
                if (obstacleTimelinePrefab != null) {
                    obstacleTimelinePrefab = null;
                }
                break;
            case EventType.changeSpeed:
                if (changeSpeedModifier != 0) {
                    changeSpeedModifier = 0;
                }
                break;
            case EventType.changeMultiplier:
                break;
            case EventType.dialog:
                if (dialogImage != null) {
                    dialogImage = null;
                }
                if (approxDialogTime != null) {
                    approxDialogTime = null;
                }
                if (dialogText != null) {
                    dialogText = null;
                }
                break;
            case EventType.uniqueEvent:
                break;
            default:
                break;
        }
    }

    private void OnEnable() {
        backUpType = eventType;
    }

    [TypeInfoBox("If you change type, data will be lost")]
    public class ChangeTypeConfirmer : OkCancelWindow {

    }

    #endregion

    #region Overrides

    public override void SetPath() {
        switch (eventType) {
            case EventType.spawnItem:
                path = spawnItemPath;
                break;
            case EventType.spawnSquad:
                path = spawnSquadPath;
                break;
            case EventType.spawnBoss:
                path = spawnBossPath;
                break;
            case EventType.spawnObstacle:
                path = spawnObstaclePath;
                break;
            case EventType.changeSpeed:
                path = changeSpeedPath;
                break;
            case EventType.changeMultiplier:
                path = changeMultiplierPath;
                break;
            case EventType.dialog:
                path = dialogPath;
                break;
            case EventType.uniqueEvent:
                path = uniqueEventPath;
                break;
            default:
                break;
        }
    }

    #region SetName

    public override void SetName() {
        if (eventType == EventType.spawnBoss) {
            eventName = factionName;
            if (enemyName != null && enemyName != "") {
                eventName += " - " + enemyName;
            }
        }
        else if (eventType == EventType.spawnSquad) {
            eventName = factionName + " - " + difficultyName;
            if (enemyName != null && enemyName != "") {
                eventName += " - " + enemyName;
            }
        }
        objectName = eventName + ".asset";
    }

    private void SetFactionName() {
        if (eventType == EventType.spawnBoss) {
            switch (bossFaction) {
                case Faction.graders:
                    factionName = "Graders";
                    break;
                case Faction.kleps:
                    factionName = "Kleps";
                    break;
                case Faction.treks:
                    factionName = "Treks";
                    break;
                default:
                    break;
            }
            return;
        }
        else if (eventType == EventType.spawnSquad) {
            switch (squadFaction) {
                case Faction.graders:
                    factionName = "Graders";
                    break;
                case Faction.kleps:
                    factionName = "Kleps";
                    break;
                case Faction.treks:
                    factionName = "Treks";
                    break;
                default:
                    break;
            }
        }
    }

    private void SetDifficultyname() {
        switch (patternDifficulty) {
            case Difficulty.easy:
                difficultyName = "e";
                break;
            case Difficulty.normal:
                difficultyName = "n";
                break;
            case Difficulty.hard:
                difficultyName = "h";
                break;
            case Difficulty.elite:
                difficultyName = "El";
                break;
            default:
                break;
        }
    }

    #endregion

    public override bool IsEachFieldInputted() {
        if (eventName == null && eventType != EventType.spawnBoss && eventType != EventType.spawnSquad) {
            return false;
        }
        switch (eventType) {
            case EventType.spawnItem:
                if (itemPrefab != null) {
                    return true;
                }
                break;
            case EventType.spawnSquad:
                if (squadPowerLevel != 0) {
                    if (unitCount != 0) {
                        if (spawnEntryDuration != 0) {
                            if (squadTimelinePrefab != null) {
                                return true;
                            }
                        }
                    }
                }
                break;
            case EventType.spawnBoss:
                if (bossPowerLevel != 0) {
                    if (bossTimelinePrefab != null) {
                        return true;
                    }
                }
                break;
            case EventType.spawnObstacle:
                if (obstaclePowerLevel != 0) {
                    if (obstacleTimelinePrefab != null) {
                        return true;
                    }

                }
                break;
            case EventType.changeSpeed:
                if (changeSpeedOption == ChangeSpeedOption.decreaseTime || changeSpeedOption == ChangeSpeedOption.increaseTime) {
                    if (changeSpeedModifier != 0) {
                        return true;
                    }
                }
                else {
                    return true;
                }
                break;
            case EventType.changeMultiplier:
                return true;
            case EventType.dialog:
                if (dialogImage != null) {
                    if (dialogText != null) {
                        return true;
                    }

                }
                break;
            case EventType.uniqueEvent:
                return true;
            default:
                break;
        }
        return false;
    }
    public override void PasteData<SOData>(SOData dataToPaste) {
        var eventToPaste = dataToPaste as EventData;
        if (eventToPaste == null) {
            Debug.Log("Pasting event failed");
            return;
        }
        eventName = eventToPaste.eventName;
        eventType = eventToPaste.eventType;
        switch (eventType) {
            case EventType.spawnItem:
                itemPrefab = eventToPaste.itemPrefab;
                break;
            case EventType.spawnSquad:
                factionName = eventToPaste.factionName;
                difficultyName = eventToPaste.difficultyName;
                enemyName = eventToPaste.enemyName;
                squadFaction = eventToPaste.squadFaction;
                squadPowerLevel = eventToPaste.squadPowerLevel;
                patternDifficulty = eventToPaste.patternDifficulty;
                unitCount = eventToPaste.unitCount;
                spawnEntryDuration = eventToPaste.spawnEntryDuration;
                stopSpawnerUntilSquadDestroyed = eventToPaste.stopSpawnerUntilSquadDestroyed;
                approxSpawnSquadTime = eventToPaste.approxSpawnSquadTime;
                squadTimelinePrefab = eventToPaste.squadTimelinePrefab;
                break;
            case EventType.spawnBoss:
                factionName = eventToPaste.factionName;
                enemyName = eventToPaste.enemyName;
                bossFaction = eventToPaste.bossFaction;
                bossPowerLevel = eventToPaste.bossPowerLevel;
                approxSpawnBossTime = eventToPaste.approxSpawnBossTime;
                bossTimelinePrefab = eventToPaste.bossTimelinePrefab;
                break;
            case EventType.spawnObstacle:
                dropResource = eventToPaste.dropResource;
                obstaclePowerLevel = eventToPaste.obstaclePowerLevel;
                approxObstacleTime = eventToPaste.approxObstacleTime;
                obstacleTimelinePrefab = eventToPaste.obstacleTimelinePrefab;
                break;
            case EventType.changeSpeed:
                changeSpeedOption = eventToPaste.changeSpeedOption;
                changeSpeedModifier = eventToPaste.changeSpeedModifier;
                break;
            case EventType.changeMultiplier:
                break;
            case EventType.dialog:
                character = eventToPaste.character;
                dialogImage = eventToPaste.dialogImage;
                approxDialogTime = eventToPaste.approxDialogTime;
                dialogType = eventToPaste.dialogType;
                dialogText = eventToPaste.dialogText;
                break;
            case EventType.uniqueEvent:
                break;
            default:
                break;
        }
    }

    #endregion

    #region GetParametersTitle

    private string GetParametersTitle() {
        switch (eventType) {
            case EventType.spawnItem:
                return "Item General";
            case EventType.spawnSquad:
                return "Squad General";
            case EventType.spawnBoss:
                return "Boss General";
            case EventType.spawnObstacle:
                return "Obstacle General";
            case EventType.changeSpeed:
                return "Change Speed";
            case EventType.changeMultiplier:
                return "Change Multiplier";
            case EventType.dialog:
                return "Dialog General";
            case EventType.uniqueEvent:
                return "Unique Event";
            default:
                return "";
        }
    }

    #endregion

    #region PropertyColorManager

    private Color GetColorEventName() { return (this.eventName == null || this.eventName == "") ? redColor : greyColor; }


    private Color GetColorItemPrefab() { return itemPrefab == null ? redColor : greyColor; }


    private Color GetColorSquadPowerLevel() { return squadPowerLevel == 0 ? redColor : greyColor; }
    private Color GetColorSquadUnitCount() { return unitCount == 0 ? redColor : greyColor; }
    private Color GetColorSquadEntryDuration() { return spawnEntryDuration == 0 ? redColor : greyColor; }
    private Color GetColorSquadTime() { return approxSpawnSquadTime.IsEmpty() ? redColor : greyColor; }
    private Color GetColorSquadPrefab() { return squadTimelinePrefab == null ? redColor : greyColor; }


    private Color GetColorBossPowerLevel() { return bossPowerLevel == 0 ? redColor : greyColor; }
    private Color GetColorBossTime() { return approxSpawnBossTime.IsEmpty() ? redColor : greyColor; }
    private Color GetColorBossPrefab() { return bossTimelinePrefab == null ? redColor : greyColor; }


    private Color GetColorObstaclePowerLevel() { return obstaclePowerLevel == 0 ? redColor : greyColor; }
    private Color GetColorObstacleTime() { return approxObstacleTime.IsEmpty() ? redColor : greyColor; }
    private Color GetColorObstaclePrefab() { return obstacleTimelinePrefab == null ? redColor : greyColor; }


    private Color GetColorChangeTimeModifier() { return changeSpeedModifier == 0 ? redColor : greyColor; }


    private Color GetColorDialogImage() { return dialogImage == null ? redColor : greyColor; }
    private Color GetColorDialogTime() { return approxDialogTime.IsEmpty() ? redColor : greyColor; }
    private Color GetColorDialogText() { return (this.dialogText == null || this.dialogText == "") ? redColor : greyColor; }

    #endregion
}

#region Enums
public enum EventType {
    spawnItem,
    spawnSquad,
    spawnBoss,
    spawnObstacle,
    changeSpeed,
    changeMultiplier,
    dialog,
    uniqueEvent
}

public enum Difficulty {
    easy,
    normal,
    hard,
    elite
}

public enum DialogType {
    small,
    large
}

public enum ChangeSpeedOption {
    increaseTime,
    decreaseTime,
    stop,
    start
}
#endregion

#region TimeContainerClass

[Serializable]
public class TimeContainer {
    [MinValue(0)]
    public int min;
    [MinValue(0)]
    public int sec;

    public bool IsEmpty() {
        return (this.min == 0 && this.sec == 0) ? true : false;
    }
}

#endregion

#region LanesClass

[Serializable]
public class Lanes {
    [HideInInspector]
    public int length = 11;
    [HideInInspector]
    public bool[] lanes = new bool[11];

    [HorizontalGroup("Lanes", 20), OnValueChanged("LanesArrayUpdate"), HideLabel]
    public bool lane1;
    [HorizontalGroup("Lanes", 20), OnValueChanged("LanesArrayUpdate"), HideLabel]
    public bool lane2;
    [HorizontalGroup("Lanes", 20), OnValueChanged("LanesArrayUpdate"), HideLabel]
    public bool lane3;
    [HorizontalGroup("Lanes", 20), OnValueChanged("LanesArrayUpdate"), HideLabel]
    public bool lane4;
    [HorizontalGroup("Lanes", 20), OnValueChanged("LanesArrayUpdate"), HideLabel]
    public bool lane5;
    [HorizontalGroup("Lanes", 20), OnValueChanged("LanesArrayUpdate"), HideLabel]
    public bool lane6;
    [HorizontalGroup("Lanes", 20), OnValueChanged("LanesArrayUpdate"), HideLabel]
    public bool lane7;
    [HorizontalGroup("Lanes", 20), OnValueChanged("LanesArrayUpdate"), HideLabel]
    public bool lane8;
    [HorizontalGroup("Lanes", 20), OnValueChanged("LanesArrayUpdate"), HideLabel]
    public bool lane9;
    [HorizontalGroup("Lanes", 20), OnValueChanged("LanesArrayUpdate"), HideLabel]
    public bool lane10;
    [HorizontalGroup("Lanes", 20), OnValueChanged("LanesArrayUpdate"), HideLabel]
    public bool lane11;

    private void LanesArrayUpdate() {
        int i = 0;
        lanes[i++] = lane1;
        lanes[i++] = lane2;
        lanes[i++] = lane3;
        lanes[i++] = lane4;
        lanes[i++] = lane5;
        lanes[i++] = lane6;
        lanes[i++] = lane7;
        lanes[i++] = lane8;
        lanes[i++] = lane9;
        lanes[i++] = lane10;
        lanes[i++] = lane11;
    }

    private void LanesTogglesUpdate() {
        int i = 0;
        lane1 = lanes[i++];
        lane2 = lanes[i++];
        lane3 = lanes[i++];
        lane4 = lanes[i++];
        lane5 = lanes[i++];
        lane6 = lanes[i++];
        lane7 = lanes[i++];
        lane8 = lanes[i++];
        lane9 = lanes[i++];
        lane10 = lanes[i++];
        lane11 = lanes[i++];
    }

    public Lanes() {
        for (int i = 0; i < this.length; i++) {
            lanes[i] = true;
        }
        LanesTogglesUpdate();
    }

    public void Copy(Lanes lanesToCopy) {
        lane1 = lanesToCopy.lane1;
        lane2 = lanesToCopy.lane2;
        lane3 = lanesToCopy.lane3;
        lane4 = lanesToCopy.lane4;
        lane5 = lanesToCopy.lane5;
        lane6 = lanesToCopy.lane6;
        lane7 = lanesToCopy.lane7;
        lane8 = lanesToCopy.lane8;
        lane9 = lanesToCopy.lane9;
        lane10 = lanesToCopy.lane10;
        lane11 = lanesToCopy.lane11;
    }
}

[Serializable]
public class LanesNumbers {
    public float indent;
    public LanesNumbers(float indent) {
        this.indent = indent;
    }
}
#endregion