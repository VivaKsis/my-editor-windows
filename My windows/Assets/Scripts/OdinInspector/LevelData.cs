﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.Utilities.Editor;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;

public class LevelData : SOData {

    #region LevelParameters

    [HorizontalGroup("Parameters", 0.5f)]

    [VerticalGroup("Parameters/Left"), GUIColor(0.8f, 0.8f, 0.8f), LabelWidth(175)]
    public LevelType levelType;
    [VerticalGroup("Parameters/Left"), GUIColor("GetColorSector"), LabelWidth(175)]
    public int sector;
    [VerticalGroup("Parameters/Left"), GUIColor("GetColorLevel"), LabelWidth(175)]
    public int level;
    [VerticalGroup("Parameters/Left"), GUIColor(0.8f, 0.8f, 0.8f), LabelWidth(175)]
    public Difficulty difficulty;
    [VerticalGroup("Parameters/Left"), GUIColor(0.8f, 0.8f, 0.8f), LabelWidth(175)]
    public List<Mission> mission = new List<Mission>();
    [VerticalGroup("Parameters/Left"), GUIColor("GetColorPowerLevel"), LabelWidth(175)]
    public int powerLevel;
    [VerticalGroup("Parameters/Left"), GUIColor(0.8f, 0.8f, 0.8f), LabelWidth(175), ReadOnly]
    public string multiplierMessage = "Not yet created";
    [VerticalGroup("Parameters/Left"), GUIColor("GetColorMaxSpawnedUnits"), LabelWidth(175)]
    public int maxSpawnedUnits;
    [VerticalGroup("Parameters/Left"), GUIColor("GetColorBackgroundScene"), LabelWidth(175)]
    public UnityEngine.Object backgroundScene;

    #endregion

    #region MissionTotals

    [TitleGroup("Parameters/Mission Totals"), GUIColor(0.8f, 0.8f, 0.8f), ReadOnly, LabelWidth(175)]
    public int unitCount;
    [TitleGroup("Parameters/Mission Totals"), GUIColor(0.8f, 0.8f, 0.8f), ReadOnly, LabelWidth(175)]
    public int obstaclesCount;
    [TitleGroup("Parameters/Mission Totals"), GUIColor(0.8f, 0.8f, 0.8f), ReadOnly, LabelWidth(175)]
    public int itemCount;
    [TitleGroup("Parameters/Mission Totals"), GUIColor(0.8f, 0.8f, 0.8f), ReadOnly, LabelWidth(175)]
    public int bosses;
    [TitleGroup("Parameters/Mission Totals"), GUIColor(0.8f, 0.8f, 0.8f), ReadOnly, LabelWidth(175)]
    public string approxMisssionTime;

    #endregion

    #region Paths

    #region LevelPaths

    private string campaignPath = "Assets/Resources/Levels/Campaign";
    private string conquestPath = "Assets/Resources/Levels/Conquest";
    private string dailyPath = "Assets/Resources/Levels/Daily";
    private string endlessPath = "Assets/Resources/Levels/Endless";

    #endregion

    #region EventPaths

    private string changeMultiplierPath = "Events/Change Multiplier";
    private string changeSpeedPath = "Events/Change Speed";
    private string dialogPath = "Events/Dialog";
    private string spawnBossPath = "Events/Spawn Boss";
    private string spawnItemPath = "Events/Spawn Item";
    private string spawnObstaclePath = "Events/Spawn Obstacle";
    private string spawnSquadPath = "Events/Spawn Squad";
    private string uniqueEventPath = "Events/Unique Event";

    #endregion

    #endregion

    #region Events

    [HorizontalGroup("Events", 0.5f)]

    [HorizontalGroup("Events/Left")]

    [InfoBox("Please change lanes the last, so they won't be lost", InfoMessageType.Warning)]
    [HorizontalGroup("Events/Left", 0.8f), GUIColor(1f, 1f, 0.75f), PropertySpace(SpaceBefore = 10), OnValueChanged("LevelEventsUpdate"), InlineProperty]
    public List<EventData> levelEvents = new List<EventData>();
    [HorizontalGroup("Events/Left", 0.2f), GUIColor(1f, 1f, 0.75f), PropertySpace(SpaceBefore = 50), LabelText("Lanes"), ListDrawerSettings(DraggableItems = false, IsReadOnly = true), InlineProperty]
    public List<FinalLanes> finalLanes = new List<FinalLanes>();

    [VerticalGroup("Events/Right"), OnValueChanged("PageUpdate"), EnumToggleButtons, GUIColor(1f, 0.85f, 1f), PropertySpace(SpaceBefore = -10), HideLabel]
    public EventType eventPage;

    [VerticalGroup("Events/Right"), OnValueChanged("PageUpdate"), GUIColor(1f, 0.85f, 1f), InlineProperty]
    public List<EventData> availableEvents;

    private void PageUpdate() {
        switch (eventPage) {
            case EventType.spawnItem:
                availableEvents = new List<EventData>(Resources.LoadAll<EventData>(spawnItemPath));
                break;
            case EventType.spawnSquad:
                availableEvents = new List<EventData>(Resources.LoadAll<EventData>(spawnSquadPath));
                break;
            case EventType.spawnBoss:
                availableEvents = new List<EventData>(Resources.LoadAll<EventData>(spawnBossPath));
                break;
            case EventType.spawnObstacle:
                availableEvents = new List<EventData>(Resources.LoadAll<EventData>(spawnObstaclePath));
                break;
            case EventType.dialog:
                availableEvents = new List<EventData>(Resources.LoadAll<EventData>(dialogPath));
                break;
            case EventType.changeSpeed:
                availableEvents = new List<EventData>(Resources.LoadAll<EventData>(changeSpeedPath));
                break;
            case EventType.changeMultiplier:
                availableEvents = new List<EventData>(Resources.LoadAll<EventData>(changeMultiplierPath));
                break;
            case EventType.uniqueEvent:
                availableEvents = new List<EventData>(Resources.LoadAll<EventData>(uniqueEventPath));
                break;
            default:
                availableEvents = new List<EventData>();
                break;
        }
    }

    private void LevelEventsUpdate() {

        EqualizeCounts();
        ConnectDataAccordinly();

        UpdateMissionTotals();
    }

    private void ConnectDataAccordinly() {
        for (int i = 0; i < levelEvents.Count; i++) {
            if (levelEvents[i] == null) {
                Debug.Log("This event is missing");
                return;
            }
            finalLanes[i].eventType = levelEvents[i].eventType;

            if (levelEvents[i].eventType == EventType.spawnSquad) {
                finalLanes[i].actualLanes = new FinalLanes.ActualLanes();
                finalLanes[i].actualLanes.actualLanes = new Lanes();
                finalLanes[i].actualLanes.actualLanes.Copy(levelEvents[i].squadLanes);
            }
            else if (levelEvents[i].eventType == EventType.spawnBoss) {
                finalLanes[i].actualLanes = new FinalLanes.ActualLanes();
                finalLanes[i].actualLanes.actualLanes = new Lanes();
                finalLanes[i].actualLanes.actualLanes.Copy(levelEvents[i].bossLanes);
            }
            else {
                finalLanes[i].actualLanes = null;
            }
        }
    }

    private void EqualizeCounts() {
        if (levelEvents.Count > finalLanes.Count) {
            while (levelEvents.Count > finalLanes.Count) {
                finalLanes.Add(new FinalLanes());
            }
        }
        else if (levelEvents.Count < finalLanes.Count) {
            while (levelEvents.Count < finalLanes.Count) {
                finalLanes.RemoveAt(0);
            }
        }
    }

    #endregion

    #region Overrides

    #region SetPath

    public override void SetPath() {
        SetTypePath();
    }

    private void SetTypePath() {
        switch (levelType) {
            case LevelType.campaign:
                path = campaignPath;
                break;
            case LevelType.endless:
                path = endlessPath;
                break;
            case LevelType.conquest:
                path = conquestPath;
                break;
            case LevelType.daily:
                path = dailyPath;
                break;
            default:
                break;
        }
        SetSectorPath();
    }

    private void SetSectorPath() {
        path = path + "/" + "Sector " + sector.ToString();
        SetLevelPath();
    }

    private void SetLevelPath() {
        path = path + "/" + "Level " + level.ToString();
    }

    public override void SetName() {
        objectName = sector.ToString() + " - " + level.ToString() + " - ";
        switch (difficulty) {
            case Difficulty.easy:
                objectName += "Easy" + ".asset";
                break;
            case Difficulty.normal:
                objectName += "Normal" + ".asset";
                break;
            case Difficulty.hard:
                objectName += "Hard" + ".asset";
                break;
            case Difficulty.elite:
                objectName += "Elite" + ".asset";
                break;
            default:
                break;
        }
    }

    #endregion

    public override bool IsEachFieldInputted() {
        if (sector != 0) {
            if (level != 0) {
                if (powerLevel != 0) {
                    if (maxSpawnedUnits != 0) {
                        if (backgroundScene != null) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public override void PasteData<SOData>(SOData dataToPaste) {
        var levelToPaste = dataToPaste as LevelData;
        if (levelToPaste == null) {
            Debug.Log("Pasting level failed");
            return;
        }
        levelType = levelToPaste.levelType;
        sector = levelToPaste.sector;
        level = levelToPaste.level;
        difficulty = levelToPaste.difficulty;
        mission = new List<Mission>();
        mission.AddRange(levelToPaste.mission);
        powerLevel = levelToPaste.powerLevel;
        maxSpawnedUnits = levelToPaste.maxSpawnedUnits;
        backgroundScene = levelToPaste.backgroundScene;

        levelEvents = new List<EventData>();
        levelEvents.AddRange(levelToPaste.levelEvents);

        finalLanes = new List<FinalLanes>();
        finalLanes.AddRange(levelToPaste.finalLanes);

        UpdateMissionTotals();
    }

    #endregion

    #region MissionMethods

    private void UpdateMissionTotals() {

        bosses = 0;
        itemCount = 0;
        obstaclesCount = 0;
        unitCount = 0;
        TimeContainer time = new TimeContainer();

        foreach (EventData e in levelEvents) {
            if (e.eventType == EventType.spawnBoss) {
                bosses++;
                time.min += e.approxSpawnBossTime.min;
                time.sec += e.approxSpawnBossTime.sec;
                continue;
            }
            if (e.eventType == EventType.spawnItem) {
                itemCount++;
                continue;
            }
            if (e.eventType == EventType.spawnObstacle) {
                time.min += e.approxObstacleTime.min;
                time.sec += e.approxObstacleTime.sec;
                obstaclesCount++;
                continue;
            }
            if (e.eventType == EventType.spawnSquad) {
                time.min += e.approxSpawnSquadTime.min;
                time.sec += e.approxSpawnSquadTime.sec;
                unitCount += e.unitCount;
                continue;
            }
            if (e.eventType == EventType.dialog) {
                time.min += e.approxDialogTime.min;
                time.sec += e.approxDialogTime.sec;
                unitCount += e.unitCount;
                continue;
            }
        }
        CountApproximateTime(time);
    }

    private void CountApproximateTime(TimeContainer time) {
        int min = time.min + time.sec / 60;
        int sec = time.sec % 60;
        approxMisssionTime = min.ToString() + " : " + sec.ToString();
    }

    #endregion

    #region MissionClass

    [Serializable]
    public class Mission {
        [LabelWidth(150), OnValueChanged("ClearMissionVariables")]
        public MissionType missionType;

        [ShowIf("@missionType == MissionType.exterminate"), GUIColor("GetColorExterminateKillCount"), LabelText("Kill Count"), LabelWidth(150), Indent]
        public int exterminateKillCount;
        [ShowIf("@missionType == MissionType.bounty"), GUIColor("GetColorBountyScoreLimit"), LabelText("Score Limit"), LabelWidth(150), Indent]
        public int bountyScoreLimit;
        [ShowIf("@missionType == MissionType.bounty"), GUIColor("GetColorBountyTimeLimit"), LabelText("Time Limit"), LabelWidth(150), Indent]
        public int bountyTimeLimit;
        [ShowIf("@missionType == MissionType.collector"), GUIColor("GetColorCollectorResourceCount"), LabelText("Resource Count"), LabelWidth(150), Indent]
        public int collectorResourceCount;
        [ShowIf("@missionType == MissionType.collector"), GUIColor(0.8f, 0.8f, 0.8f), LabelText("Resource Type"), ReadOnly, LabelWidth(150), Indent]
        public string collectorResourceType = "Not yet created";
        [ShowIf("@missionType == MissionType.collector"), GUIColor("GetColorCollectorTimeLimit"), LabelText("Time Limit"), LabelWidth(150), Indent]
        public int collectorTimeLimit;
        [ShowIf("@missionType == MissionType.assassination"), GUIColor("GetColorAssassinationPowerLevelIncrease"), LabelText("Power Level Increase"), LabelWidth(150), Indent]
        public float assassinationPowerLevelIncrease;
        [ShowIf("@missionType == MissionType.assassination"), GUIColor("GetColorAssassinationScaleSizeIncrease"), LabelText("Scale Size Increase"), LabelWidth(150), Indent]
        public float assassinationScaleSizeIncrease;

        private void ClearMissionVariables() {
            if (exterminateKillCount != 0) {
                exterminateKillCount = 0;
            }
            if (bountyScoreLimit != 0) {
                bountyScoreLimit = 0;
            }
            if (bountyTimeLimit != 0) {
                bountyTimeLimit = 0;
            }
            if (collectorResourceCount != 0) {
                collectorResourceCount = 0;
            }
            if (collectorTimeLimit != 0) {
                collectorTimeLimit = 0;
            }
            if (assassinationPowerLevelIncrease != 0) {
                assassinationPowerLevelIncrease = 0;
            }
            if (assassinationScaleSizeIncrease != 0) {
                assassinationScaleSizeIncrease = 0;
            }
        }

        #region PropertyColorManager
        private Color GetColorExterminateKillCount() { return this.exterminateKillCount == 0 ? redColor : greyColor; }
        private Color GetColorBountyScoreLimit() { return this.bountyScoreLimit == 0 ? redColor : greyColor; }
        private Color GetColorBountyTimeLimit() { return this.bountyTimeLimit == 0 ? redColor : greyColor; }
        private Color GetColorCollectorResourceCount() { return this.collectorResourceCount == 0 ? redColor : greyColor; }
        private Color GetColorCollectorTimeLimit() { return this.collectorTimeLimit == 0 ? redColor : greyColor; }
        private Color GetColorAssassinationPowerLevelIncrease() { return this.assassinationPowerLevelIncrease == 0 ? redColor : greyColor; }
        private Color GetColorAssassinationScaleSizeIncrease() { return this.assassinationScaleSizeIncrease == 0 ? redColor : greyColor; }

        #endregion
    }

    #endregion

    #region FinalLanesClass

    [Serializable]
    public class FinalLanes {

        [HideInInspector]
        public EventType eventType;

        [HideInInspector]
        public NeitherSquadNorBossError neitherSquadNorBossError;
        [HideInInspector]
        public ActualLanes actualLanes;

        [Button("Lane", ButtonSizes.Small)]
        public void Overridelanes() {
            if (eventType != EventType.spawnSquad && eventType != EventType.spawnBoss) {
                neitherSquadNorBossError = new NeitherSquadNorBossError();
                CreatePopUpWindow(ref neitherSquadNorBossError);
                actualLanes = null;
                return;
            }
            CreatePopUpWindow(ref actualLanes);
        }

        #region CreatePopUpWindow
        private void CreatePopUpWindow<T>(ref T popUpWindow) where T : PopUpWindow, new() {
            if (popUpWindow != null) {
                OdinEditorWindow window = OdinEditorWindow.InspectObject(popUpWindow);
                window.position = GUIHelper.GetEditorWindowRect().AlignCenter(270, 100);
                popUpWindow.window = window;
            }
        }

        #endregion

        #region PopUpClasses

        [TypeInfoBox("It's neither a squad not a boss event")]
        public class NeitherSquadNorBossError : OKWindow {

        }

        [Serializable]
        public class ActualLanes : PopUpWindow {

            [InlineProperty, HideLabel]
            public LanesNumbers lanesNumbers = new LanesNumbers(40f);
            [InlineProperty, LabelText("Lanes"), LabelWidth(42)]
            public Lanes actualLanes;

            [Button(ButtonSizes.Large)]
            [GUIColor(0.75f, 1f, 1f), PropertySpace]
            public void Save() {
                window.Close();
            }
        }

        #endregion
    }

    #endregion

    #region PropertyColorManager
    private Color GetColorSector() { return this.sector == 0 ? redColor : greyColor; }
    private Color GetColorLevel() { return this.level == 0 ? redColor : greyColor; }
    private Color GetColorPowerLevel() { return this.powerLevel == 0 ? redColor : greyColor; }
    private Color GetColorMaxSpawnedUnits() { return this.maxSpawnedUnits == 0 ? redColor : greyColor; }
    private Color GetColorBackgroundScene() { return this.backgroundScene == null ? redColor : greyColor; }
    
    #endregion
}

#region Enums
public enum LevelType {
    campaign,
    endless,
    conquest,
    daily
}

public enum MissionType {
    exterminate,
    bounty,
    hellFire,
    payloadDelivery,
    hotDrop,
    escortMission,
    collector,
    assassination
}
#endregion

