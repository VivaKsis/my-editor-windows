﻿using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "EnemyShipData", menuName = "SO Data/Enemy Ship Data")]
public class EnemyShipData : SOData {

    #region GeneralParameters

    [HorizontalGroup("Parameters")]
    [BoxGroup("Parameters/General")]

    [HorizontalGroup("Parameters/General/Left", 75), PreviewField(75), HideLabel, GUIColor("GetColorImage")]
    public Sprite image;
    [VerticalGroup("Parameters/General/Left/Right"), LabelWidth(110), GUIColor(0.8f, 0.8f, 0.8f)]
    public Faction faction;
    [VerticalGroup("Parameters/General/Left/Right"), LabelWidth(110), GUIColor("GetColorName")]
    public string shipName;
    [VerticalGroup("Parameters/General/Left/Right"), LabelWidth(110), GUIColor(0.8f, 0.8f, 0.8f)]
    public EnemyShipClass enemyShipClass;

    #endregion

    #region Stats

    [FoldoutGroup("Stats")]

    [TitleGroup("Stats/Ship")]
    [HorizontalGroup("Stats/Ship/General", 0.5f)]

    [VerticalGroup("Stats/Ship/General/Left"), LabelWidth(100), GUIColor("GetColorHealth")]
    public float health;
    [VerticalGroup("Stats/Ship/General/Left"), LabelWidth(100), GUIColor("GetColorArmor")]
    public float armor;

    [VerticalGroup("Stats/Ship/General/Right"), LabelWidth(130), GUIColor("GetColorShield")]
    public float shield;
    [VerticalGroup("Stats/Ship/General/Right"), LabelWidth(130), GUIColor(0.8f, 0.8f, 0.8f)]
    public float shieldRegenSpeed;
    [VerticalGroup("Stats/Ship/General/Right"), LabelWidth(130), GUIColor(0.8f, 0.8f, 0.8f)]
    public float shieldDelayRegen;

    [TitleGroup("Stats/Weapon")]
    [HorizontalGroup("Stats/Weapon/General", 0.5f)]

    [VerticalGroup("Stats/Weapon/General/Left"), LabelWidth(100), GUIColor("GetColorDamage")]
    public float damage;
    [VerticalGroup("Stats/Weapon/General/Left"), LabelWidth(100), GUIColor("GetColorMagazineCap")]
    public float magazineCap;
    [VerticalGroup("Stats/Weapon/General/Left"), LabelWidth(100), GUIColor("GetColorFireRate")]
    public float fireRate;
    [VerticalGroup("Stats/Weapon/General/Left"), LabelWidth(100), GUIColor("GetColorProjectileSpeed")]
    public float projectileSpeed;
    [VerticalGroup("Stats/Weapon/General/Left"), LabelWidth(100), GUIColor(0.8f, 0.8f, 0.8f)]
    public float lifeSteal;

    [VerticalGroup("Stats/Weapon/General/Right"), LabelWidth(130), GUIColor("GetColorCD")]
    public float CD;
    [VerticalGroup("Stats/Weapon/General/Right"), LabelWidth(130), GUIColor("GetColorCC")]
    public float CC;
    [VerticalGroup("Stats/Weapon/General/Right"), LabelWidth(130), GUIColor("GetColorRange")]
    public float range;
    [VerticalGroup("Stats/Weapon/General/Right"), LabelWidth(130), GUIColor(0.8f, 0.8f, 0.8f)]
    public float reloadSpeed;

    #endregion

    #region Modifiers

    [FoldoutGroup("Modifiers"), LabelWidth(100), GUIColor("GetColorOffense")]
    public float offense;
    [FoldoutGroup("Modifiers"), LabelWidth(100), GUIColor("GetColorDefense")]
    public float defense;
    [FoldoutGroup("Modifiers"), LabelWidth(100), GUIColor("GetColorUtility")]
    public float utility;

    #endregion

    #region Description

    [FoldoutGroup("Description"), TextArea, LabelWidth(150), HideLabel, GUIColor(0.8f, 0.8f, 0.8f)]
    public string description;

    #endregion

    #region Path

    private string enemyShipPath = "Assets/Prefab/EnemyShips";

    #endregion

    #region Overrides

    public override void SetPath() {
        path = enemyShipPath;
    }
    public override void SetName() {
        objectName = shipName + ".asset";

    }
    public override bool IsEachFieldInputted() {
        if (image != null) {
            if (shipName != null) {
                if (health != 0) {
                    if (armor != 0) {
                        if (shield != 0) {
                            if (damage != 0) {
                                if (magazineCap != 0) {
                                    if (fireRate != 0) {
                                        if (projectileSpeed != 0) {
                                            if (CD != 0) {
                                                if (CC != 0) {
                                                    if (range != 0) {
                                                        if (offense != 0) {
                                                            if (defense != 0) {
                                                                if (utility != 0) {
                                                                    objectName = shipName;
                                                                    return true;
                                                                }
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public override void PasteData<SOData>(SOData dataToPaste) {
        var enemyShipToPaste = dataToPaste as EnemyShipData;
        if (enemyShipToPaste == null) {
            Debug.Log("Pasting level failed");
            return;
        }

        image = enemyShipToPaste.image;
        faction = enemyShipToPaste.faction;
        shipName = enemyShipToPaste.shipName;
        enemyShipClass = enemyShipToPaste.enemyShipClass;

        health = enemyShipToPaste.health;
        armor = enemyShipToPaste.armor;
        shield = enemyShipToPaste.shield;
        shieldRegenSpeed = enemyShipToPaste.shieldRegenSpeed;
        shieldDelayRegen = enemyShipToPaste.shieldDelayRegen;
        CD = enemyShipToPaste.CD;
        CC = enemyShipToPaste.CC;
        range = enemyShipToPaste.range;
        reloadSpeed = enemyShipToPaste.reloadSpeed;
        damage = enemyShipToPaste.damage;
        lifeSteal = enemyShipToPaste.lifeSteal;
        magazineCap = enemyShipToPaste.magazineCap;
        fireRate = enemyShipToPaste.fireRate;
        projectileSpeed = enemyShipToPaste.projectileSpeed;

        offense = enemyShipToPaste.offense;
        defense = enemyShipToPaste.defense;
        utility = enemyShipToPaste.utility;

        description = enemyShipToPaste.description;
    }

    #endregion

    #region PropertyColorManager

    private Color GetColorImage() { return this.image == null ? redColor : greyColor; }


    private Color GetColorName() { return (this.shipName == null || this.shipName == "") ? redColor : greyColor; }


    private Color GetColorHealth() { return this.health == 0 ? redColor : greyColor; }
    private Color GetColorArmor() { return this.armor == 0 ? redColor : greyColor; }
    private Color GetColorShield() { return this.shield == 0 ? redColor : greyColor; }
    private Color GetColorCD() { return this.CD == 0 ? redColor : greyColor; }
    private Color GetColorCC() { return this.CC == 0 ? redColor : greyColor; }
    private Color GetColorRange() { return this.range == 0 ? redColor : greyColor; }
    private Color GetColorDamage() { return this.damage == 0 ? redColor : greyColor; }
    private Color GetColorMagazineCap() { return this.magazineCap == 0 ? redColor : greyColor; }
    private Color GetColorFireRate() { return this.fireRate == 0 ? redColor : greyColor; }
    private Color GetColorProjectileSpeed() { return this.projectileSpeed == 0 ? redColor : greyColor; }


    private Color GetColorOffense() { return this.offense == 0 ? redColor : greyColor; }
    private Color GetColorDefense() { return this.defense == 0 ? redColor : greyColor; }
    private Color GetColorUtility() { return this.utility == 0 ? redColor : greyColor; }

    #endregion
}

#region Enums

public enum Faction {
    treks,
    graders,
    kleps
}

public enum EnemyShipClass {
    lightFighter,
    heavyFighter,
    kamikaze,
    ace,
    heavyBoomer
}

#endregion
