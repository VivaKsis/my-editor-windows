﻿using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "PickUpsData", menuName = "SO Data/Pick Ups Data")]
public class PickUpsData : SOData {

    #region GeneralParameters
    [HorizontalGroup("Parameters")]
    [BoxGroup("Parameters/General")]

    [HorizontalGroup("Parameters/General/Left", 75), PreviewField(75), HideLabel, GUIColor("GetColorImage")]
    public Sprite image;
    [VerticalGroup("Parameters/General/Left/Right"), LabelWidth(110), GUIColor("GetColorName")]
    public string pickUpName;
    [VerticalGroup("Parameters/General/Left/Right"), LabelWidth(110), GUIColor(0.7f, 0.7f, 0.7f)]
    public PickUps pickUpType;
    #endregion

    #region Stats
    [FoldoutGroup("Stats"), LabelWidth(100), GUIColor("GetColorRate")]
    [Range(0, 100)]
    public float dropRate;
    [FoldoutGroup("Stats"), LabelWidth(100), GUIColor("GetColorCount")]
    public float dropCount;
    #endregion

    #region Path

    private string pickUpPath = "Assets/Prefab/PickUps";

    #endregion

    #region Overrides

    public override void SetPath() {
        path = pickUpPath;
    }

    public override void SetName() {
        objectName = pickUpName + ".asset";
    }

    public override bool IsEachFieldInputted() {
        if (image != null) {
            if (pickUpName != null) {
                if (dropRate != 0) {
                    if (dropCount != 0) {
                        objectName = pickUpName;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public override void PasteData<SOData>(SOData dataToPaste) {
        var pickUpToPaste = dataToPaste as PickUpsData;
        if (pickUpToPaste == null) {
            Debug.Log("Pasting pick up failed");
            return;
        }
        image = pickUpToPaste.image;
        pickUpName = pickUpToPaste.pickUpName;
        pickUpType = pickUpToPaste.pickUpType;
        dropRate = pickUpToPaste.dropRate;
        dropCount = pickUpToPaste.dropCount;
    }

    #endregion

    #region PropertyColorManager

    private Color GetColorImage() { return this.image == null ? redColor : greyColor; }
    private Color GetColorName() { return (this.pickUpName == null || this.pickUpName == "") ? redColor : greyColor; }
    private Color GetColorRate() { return this.dropRate == 0 ? redColor : greyColor; }
    private Color GetColorCount() { return this.dropCount == 0 ? redColor : greyColor; }

    #endregion
}

#region Enums

public enum PickUps {
    currency,
    resource,
    pickup
}

#endregion