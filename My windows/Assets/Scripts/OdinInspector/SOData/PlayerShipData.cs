﻿using UnityEngine;
using Sirenix.OdinInspector;
using System;
using UnityEditor;
using System.IO;

[CreateAssetMenu(fileName = "PlayerShipData", menuName = "SO Data/Player Ship Data")]
public class PlayerShipData : SOData {

    #region GeneralParameters
    [HorizontalGroup("Parameters")]
    [BoxGroup("Parameters/General")]

    [HorizontalGroup("Parameters/General/Left", 75), PreviewField(75), HideLabel, GUIColor("GetColorImage")]
    public Sprite image;
    [VerticalGroup("Parameters/General/Left/Right"), LabelWidth(110), GUIColor("GetColorName")]
    public string shipName;
    [VerticalGroup("Parameters/General/Left/Right"), LabelWidth(110), GUIColor(0.8f, 0.8f, 0.8f)]
    public PlayerShipClass playerShipClass;
    [VerticalGroup("Parameters/General/Left/Right"), LabelWidth(110), GUIColor("GetColorTokens")]
    public int tokensToUnlock;
    #endregion

    #region Components

    [FoldoutGroup("Components"), LabelWidth(150), GUIColor(0.75f, 1f, 1f), OnValueChanged("SumComponentsStatsToBonuses")]
    [AssetSelector(Paths = "Assets/Prefab/ShipComponents")]
    public ShipComponentData[] AllComponents;

    public void SumComponentsStatsToBonuses() {
        ClearBonuses();
        foreach (ShipComponentData component in AllComponents) {
            AddStatsFromComponent(component);
        }
        BonusesToString();
    }

    private void AddStatsFromComponent(ShipComponentData component) {
        AddStat(component.statList.primaryStat);
        if (component.statList.secondaryStat1 != null) {
            AddStat(component.statList.secondaryStat1);
        }
        if (component.statList.secondaryStat2 != null) {
            AddStat(component.statList.secondaryStat2);
        }
        if (component.statList.secondaryStat3 != null) {
            AddStat(component.statList.secondaryStat3);
        }
        if (component.statList.secondaryStat4 != null) {
            AddStat(component.statList.secondaryStat4);
        }
    }

    private void AddStat(ShipComponentData.Stat stat) {
        switch (stat.name) {
            #region FloatStats
            case "Health float":
                healthBonus.value += stat.value;
                break;
            case "Armor float":
                armorBonus.value += stat.value;
                break;
            case "Shield float":
                shieldBonus.value += stat.value;
                break;
            case "Shield Regen Speed float":
                shieldRegenSpeedBonus.value += stat.value;
                break;
            case "Cargo Cap float":
                cargoCapBonus.value += stat.value;
                break;
            case "Shield Regen Delay float":
                shieldDelayRegenBonus.value += stat.value;
                break;
            case "CD float":
                CDBonus.value += stat.value;
                break;
            case "CC float":
                CCBonus.value += stat.value;
                break;
            case "Range float":
                rangeBonus.value += stat.value;
                break;
            case "Reload Speed float":
                reloadSpeedBonus.value += stat.value;
                break;
            case "Damage float":
                damageBonus.value += stat.value;
                break;
            case "LifeSteal float":
                lifeStealBonus.value += stat.value;
                break;
            case "Mag. Cap float":
                magazineCapBonus.value += stat.value;
                break;
            case "FireRate float":
                fireRateBonus.value += stat.value;
                break;
            case "Projectile Speed float":
                projectileSpeedBonus.value += stat.value;
                break;
            case "Heat Resistance float":
                heatResBonus.value += stat.value;
                break;
            case "Cold Resistance float":
                coldResBonus.value += stat.value;
                break;
            case "Acid Resistance float":
                acidResBonus.value += stat.value;
                break;
            case "Electric Resistance float":
                electricResBonus.value += stat.value;
                break;
            #endregion

            #region %Stats
            case "Health %":
                healthBonus.value += health * (stat.value / 100);
                break;
            case "Armor %":
                armorBonus.value += armor * (stat.value / 100);
                break;
            case "Shield %":
                shieldBonus.value += shield * (stat.value / 100);
                break;
            case "Shield Regen Speed %":
                shieldRegenSpeedBonus.value += shieldRegenSpeed * (stat.value / 100);
                break;
            case "Cargo Cap %":
                cargoCapBonus.value += cargoCap * (stat.value / 100);
                break;
            case "Shield Regen Delay %":
                shieldDelayRegenBonus.value += shieldDelayRegen * (stat.value / 100);
                break;
            case "CD %":
                CDBonus.value += CD * (stat.value / 100);
                break;
            case "CC %":
                CCBonus.value += CC * (stat.value / 100);
                break;
            case "Range %":
                rangeBonus.value += range * (stat.value / 100);
                break;
            case "Reload Speed %":
                reloadSpeedBonus.value += reloadSpeed * (stat.value / 100);
                break;
            case "Damage %":
                damageBonus.value += damage * (stat.value / 100);
                break;
            case "LifeSteal %":
                lifeStealBonus.value += lifeSteal * (stat.value / 100);
                break;
            case "Mag. Cap %":
                magazineCapBonus.value += magazineCap * (stat.value / 100);
                break;
            case "FireRate %":
                fireRateBonus.value += fireRate * (stat.value / 100);
                break;
            case "Projectile Speed %":
                projectileSpeedBonus.value += projectileSpeed * (stat.value / 100);
                break;
            case "Heat Resistance %":
                heatResBonus.value += heatRes * (stat.value / 100);
                break;
            case "Cold Resistance %":
                coldResBonus.value += coldRes * (stat.value / 100);
                break;
            case "Acid Resistance %":
                acidResBonus.value += acidRes * (stat.value / 100);
                break;
            case "Electric Resistance %":
                electricResBonus.value += electricRes * (stat.value / 100);
                break;
            #endregion

            default:
                Debug.Log("Problem with: " + stat.name);
                break;
        }
    }

    private void ClearBonuses() {
        healthBonus.value = 0;
        armorBonus.value = 0;
        cargoCapBonus.value = 0;

        shieldBonus.value = 0;
        shieldRegenSpeedBonus.value = 0;
        shieldDelayRegenBonus.value = 0;

        damageBonus.value = 0;
        magazineCapBonus.value = 0;
        fireRateBonus.value = 0;
        projectileSpeedBonus.value = 0;
        lifeStealBonus.value = 0;

        CDBonus.value = 0;
        CCBonus.value = 0;
        rangeBonus.value = 0;
        reloadSpeedBonus.value = 0;

        heatResBonus.value = 0;
        coldResBonus.value = 0;

        acidResBonus.value = 0;
        electricResBonus.value = 0;
    }

    private void BonusesToString() {
        healthBonus.ValueToString();
        armorBonus.ValueToString();
        cargoCapBonus.ValueToString();

        shieldBonus.ValueToString();
        shieldRegenSpeedBonus.ValueToString();
        shieldDelayRegenBonus.ValueToString();

        damageBonus.ValueToString();
        magazineCapBonus.ValueToString();
        fireRateBonus.ValueToString();
        projectileSpeedBonus.ValueToString();
        lifeStealBonus.ValueToString();

        CDBonus.ValueToString();
        CCBonus.ValueToString();
        rangeBonus.ValueToString();
        reloadSpeedBonus.ValueToString();

        heatResBonus.ValueToString();
        coldResBonus.ValueToString();

        acidResBonus.ValueToString();
        electricResBonus.ValueToString();
    }

    #endregion

    #region Stats

    [FoldoutGroup("Stats")]

    [TitleGroup("Stats/Ship")]
    [HorizontalGroup("Stats/Ship/General", 0.5f)]

    [HorizontalGroup("Stats/Ship/General/Left", 0.7f)]

    [VerticalGroup("Stats/Ship/General/Left/Stat"), LabelWidth(100), GUIColor("GetColorHealth")]
    public float health;
    [VerticalGroup("Stats/Ship/General/Left/Bonus"), HideLabel]
    public ComponentBonus healthBonus = new ComponentBonus();
    [VerticalGroup("Stats/Ship/General/Left/Stat"), LabelWidth(100), GUIColor("GetColorArmor")]
    public float armor;
    [VerticalGroup("Stats/Ship/General/Left/Bonus"), HideLabel]
    public ComponentBonus armorBonus = new ComponentBonus();
    [VerticalGroup("Stats/Ship/General/Left/Stat"), LabelWidth(100), GUIColor("GetColorCargoCap")]
    public float cargoCap;
    [VerticalGroup("Stats/Ship/General/Left/Bonus"), HideLabel]
    public ComponentBonus cargoCapBonus = new ComponentBonus();

    [HorizontalGroup("Stats/Ship/General/Right", 0.75f)]

    [VerticalGroup("Stats/Ship/General/Right/Stat"), LabelWidth(130), GUIColor("GetColorShield")]
    public float shield;
    [VerticalGroup("Stats/Ship/General/Right/Bonus"), HideLabel]
    public ComponentBonus shieldBonus = new ComponentBonus();
    [VerticalGroup("Stats/Ship/General/Right/Stat"), LabelWidth(130), GUIColor(0.8f, 0.8f, 0.8f)]
    public float shieldRegenSpeed;
    [VerticalGroup("Stats/Ship/General/Right/Bonus"), HideLabel]
    public ComponentBonus shieldRegenSpeedBonus = new ComponentBonus();
    [VerticalGroup("Stats/Ship/General/Right/Stat"), LabelWidth(130), GUIColor(0.8f, 0.8f, 0.8f)]
    public float shieldDelayRegen;
    [VerticalGroup("Stats/Ship/General/Right/Bonus"), HideLabel]
    public ComponentBonus shieldDelayRegenBonus = new ComponentBonus();

    [TitleGroup("Stats/Weapon")]
    [HorizontalGroup("Stats/Weapon/General", 0.5f)]

    [HorizontalGroup("Stats/Weapon/General/Left", 0.7f)]

    [VerticalGroup("Stats/Weapon/General/Left/Stat"), LabelWidth(100), GUIColor("GetColorDamage")]
    public float damage;
    [VerticalGroup("Stats/Weapon/General/Left/Bonus"), HideLabel]
    public ComponentBonus damageBonus = new ComponentBonus();
    [VerticalGroup("Stats/Weapon/General/Left/Stat"), LabelWidth(100), GUIColor("GetColorMagazineCap")]
    public float magazineCap;
    [VerticalGroup("Stats/Weapon/General/Left/Bonus"), HideLabel]
    public ComponentBonus magazineCapBonus = new ComponentBonus();
    [VerticalGroup("Stats/Weapon/General/Left/Stat"), LabelWidth(100), GUIColor("GetColorFireRate")]
    public float fireRate;
    [VerticalGroup("Stats/Weapon/General/Left/Bonus"), HideLabel]
    public ComponentBonus fireRateBonus = new ComponentBonus();
    [VerticalGroup("Stats/Weapon/General/Left/Stat"), LabelWidth(100), GUIColor("GetColorProjectileSpeed")]
    public float projectileSpeed;
    [VerticalGroup("Stats/Weapon/General/Left/Bonus"), HideLabel]
    public ComponentBonus projectileSpeedBonus = new ComponentBonus();
    [VerticalGroup("Stats/Weapon/General/Left/Stat"), LabelWidth(100), GUIColor(0.8f, 0.8f, 0.8f)]
    public float lifeSteal;
    [VerticalGroup("Stats/Weapon/General/Left/Bonus"), HideLabel]
    public ComponentBonus lifeStealBonus = new ComponentBonus();

    [HorizontalGroup("Stats/Weapon/General/Right", 0.75f)]

    [VerticalGroup("Stats/Weapon/General/Right/Stat"), LabelWidth(130), GUIColor("GetColorCD")]
    public float CD;
    [VerticalGroup("Stats/Weapon/General/Right/Bonus"), HideLabel]
    public ComponentBonus CDBonus = new ComponentBonus();
    [VerticalGroup("Stats/Weapon/General/Right/Stat"), LabelWidth(130), GUIColor("GetColorCC")]
    public float CC;
    [VerticalGroup("Stats/Weapon/General/Right/Bonus"), HideLabel]
    public ComponentBonus CCBonus = new ComponentBonus();
    [VerticalGroup("Stats/Weapon/General/Right/Stat"), LabelWidth(130), GUIColor("GetColorRange")]
    public float range;
    [VerticalGroup("Stats/Weapon/General/Right/Bonus"), HideLabel]
    public ComponentBonus rangeBonus = new ComponentBonus();
    [VerticalGroup("Stats/Weapon/General/Right/Stat"), LabelWidth(130), GUIColor("GetColorReloadSpeed")]
    public float reloadSpeed;
    [VerticalGroup("Stats/Weapon/General/Right/Bonus"), HideLabel]
    public ComponentBonus reloadSpeedBonus = new ComponentBonus();

    [TitleGroup("Stats/Resistance")]
    [HorizontalGroup("Stats/Resistance/General", 0.5f)]

    [HorizontalGroup("Stats/Resistance/General/Left", 0.7f)]

    [VerticalGroup("Stats/Resistance/General/Left/Stat"), LabelWidth(100), GUIColor(0.8f, 0.8f, 0.8f)]
    public float heatRes;
    [VerticalGroup("Stats/Resistance/General/Left/Bonus"), HideLabel]
    public ComponentBonus heatResBonus = new ComponentBonus();
    [VerticalGroup("Stats/Resistance/General/Left/Stat"), LabelWidth(100), GUIColor(0.8f, 0.8f, 0.8f)]
    public float coldRes;
    [VerticalGroup("Stats/Resistance/General/Left/Bonus"), HideLabel]
    public ComponentBonus coldResBonus = new ComponentBonus();

    [HorizontalGroup("Stats/Resistance/General/Right", 0.75f)]

    [VerticalGroup("Stats/Resistance/General/Right/Stat"), LabelWidth(130), GUIColor(0.8f, 0.8f, 0.8f)]
    public float acidRes;
    [VerticalGroup("Stats/Resistance/General/Right/Bonus"), HideLabel]
    public ComponentBonus acidResBonus = new ComponentBonus();
    [VerticalGroup("Stats/Resistance/General/Right/Stat"), LabelWidth(130), GUIColor(0.8f, 0.8f, 0.8f)]
    public float electricRes;
    [VerticalGroup("Stats/Resistance/General/Right/Bonus"), HideLabel]
    public ComponentBonus electricResBonus = new ComponentBonus();

    #endregion

    #region Modifiers
    [FoldoutGroup("Modifiers"), LabelWidth(100), GUIColor("GetColorOffense")]
    public float offense;
    [FoldoutGroup("Modifiers"), LabelWidth(100), GUIColor("GetColorDefense")]
    public float defense;
    [FoldoutGroup("Modifiers"), LabelWidth(100), GUIColor("GetColorUtility")]
    public float utility;
    #endregion

    #region Description
    [FoldoutGroup("Description"), TextArea, LabelWidth(150), HideLabel, GUIColor(0.8f, 0.8f, 0.8f)]
    public string description;
    #endregion

    #region Path
    private string playerShipPath = "Assets/Prefab/PlayerShips";
    #endregion

    #region Overrides

    public override void SetPath() {
        path = playerShipPath;
    }
    public override void SetName() {
        objectName = shipName + ".asset";
    }
    public override bool IsEachFieldInputted() {
        if (image != null) {
            if (shipName != null) {
                if (tokensToUnlock != 0) {
                    if (health != 0) {
                        if (armor != 0) {
                            if (cargoCap != 0) {
                                if (shield != 0) {
                                    if (damage != 0) {
                                        if (magazineCap != 0) {
                                            if (fireRate != 0) {
                                                if (projectileSpeed != 0) {
                                                    if (CD != 0) {
                                                        if (CC != 0) {
                                                            if (range != 0) {
                                                                if (reloadSpeed != 0) {
                                                                    if (offense != 0) {
                                                                        if (defense != 0) {
                                                                            if (utility != 0) {
                                                                                objectName = shipName;
                                                                                return true;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return false;
    }

    public override void PasteData<SOData>(SOData dataToPaste) {
        var playerShipToPaste = dataToPaste as PlayerShipData;
        if (playerShipToPaste == null) {
            Debug.Log("Pasting level failed");
            return;
        }

        image = playerShipToPaste.image;
        shipName = playerShipToPaste.shipName;
        playerShipClass = playerShipToPaste.playerShipClass;
        tokensToUnlock = playerShipToPaste.tokensToUnlock;

        AllComponents = playerShipToPaste.AllComponents;

        health = playerShipToPaste.health;
        armor = playerShipToPaste.armor;
        shield = playerShipToPaste.shield;
        shieldRegenSpeed = playerShipToPaste.shieldRegenSpeed;
        cargoCap = playerShipToPaste.cargoCap;
        shieldDelayRegen = playerShipToPaste.shieldDelayRegen;
        CD = playerShipToPaste.CD;
        CC = playerShipToPaste.CC;
        range = playerShipToPaste.range;
        reloadSpeed = playerShipToPaste.reloadSpeed;
        damage = playerShipToPaste.damage;
        lifeSteal = playerShipToPaste.lifeSteal;
        magazineCap = playerShipToPaste.magazineCap;
        fireRate = playerShipToPaste.fireRate;
        projectileSpeed = playerShipToPaste.projectileSpeed;
        heatRes = playerShipToPaste.heatRes;
        coldRes = playerShipToPaste.coldRes;
        acidRes = playerShipToPaste.acidRes;
        electricRes = playerShipToPaste.electricRes;

        offense = playerShipToPaste.offense;
        defense = playerShipToPaste.defense;
        utility = playerShipToPaste.utility;

        description = playerShipToPaste.description;
    }

    #endregion

    #region ComponentBonus

    [Serializable]
    public class ComponentBonus {
        [HideInInspector]
        public float value = 0;

        [GUIColor(0.8f, 0.8f, 0.8f), HideLabel, ReadOnly]
        public string valueString;

        public void ValueToString() {
            valueString = "+" + value.ToString();
        }
    }

    #endregion

    #region PropertyColorManager

    private Color GetColorImage() { return this.image == null ? redColor : greyColor; }
    private Color GetColorName() { return (this.shipName == null || this.shipName == "") ? redColor : greyColor; }
    private Color GetColorTokens() { return this.tokensToUnlock == 0 ? redColor : greyColor; }


    private Color GetColorHealth() { return this.health == 0 ? redColor : greyColor; }
    private Color GetColorArmor() { return this.armor == 0 ? redColor : greyColor; }
    private Color GetColorShield() { return this.shield == 0 ? redColor : greyColor; }
    private Color GetColorCargoCap() { return this.cargoCap == 0 ? redColor : greyColor; }
    private Color GetColorCD() { return this.CD == 0 ? redColor : greyColor; }
    private Color GetColorCC() { return this.CC == 0 ? redColor : greyColor; }
    private Color GetColorRange() { return this.range == 0 ? redColor : greyColor; }
    private Color GetColorReloadSpeed() { return this.reloadSpeed == 0 ? redColor : greyColor; }
    private Color GetColorDamage() { return this.damage == 0 ? redColor : greyColor; }
    private Color GetColorMagazineCap() { return this.magazineCap == 0 ? redColor : greyColor; }
    private Color GetColorFireRate() { return this.fireRate == 0 ? redColor : greyColor; }
    private Color GetColorProjectileSpeed() { return this.projectileSpeed == 0 ? redColor : greyColor; }


    private Color GetColorOffense() { return this.offense == 0 ? redColor : greyColor; }
    private Color GetColorDefense() { return this.defense == 0 ? redColor : greyColor; }
    private Color GetColorUtility() { return this.utility == 0 ? redColor : greyColor; }

    #endregion
}

#region Enums

public enum PlayerShipClass {
    tank,
    support,
    cargo,
    fighter
}

#endregion
